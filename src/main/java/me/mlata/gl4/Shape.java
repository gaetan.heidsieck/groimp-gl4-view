package me.mlata.gl4;

public class Shape {
    public static final int SPHERE = 0;
    public static final int CUBE = 1;
    public static final int CYLINDER = 2;

    public static final int SHAPE_COUNT = 3;
}
