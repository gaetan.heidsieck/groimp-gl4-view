package me.mlata.gl4;

import com.jogamp.opengl.GL;
import com.jogamp.opengl.GL4;

public class VertexArrayObject {
    Buffer vertices;
    Buffer normals;
    Buffer indices;
    int id;

    // the type of OpenGL primitive to use
    Primitive primitive;
    // whether to use billboarding when rendering
    boolean billboarding;
    // whether to calculate shading when rendering
    boolean shading;
    // whether to only draw the wireframe
    boolean wireframe;

    static final int POSITION_ATTRIB_INDEX = 0;
    static final int NORMAL_ATTRIB_INDEX = 1;

    static final int SHADING_UNIFORM_LOCATION = 4;
    static final int BILLBOARDING_UNIFORM_LOCATION = 5;

    public void bind(GL4 gl) {
        // this code should be enough, but it stops rendering after one frame for some reason
        // gl.glBindVertexArray(this.id);
        // indices.bind(gl, BufferTarget.ELEMENT_ARRAY);

        // TODO: get rid of this garbage
        // bind VAO
        gl.glBindVertexArray(this.id);
        // Add position buffer
        vertices.bind(gl, BufferTarget.ARRAY);
        gl.glVertexAttribPointer(POSITION_ATTRIB_INDEX, 3, GL.GL_FLOAT, false, 0, 0);
        gl.glEnableVertexAttribArray(POSITION_ATTRIB_INDEX);
        if (this.normals != null && this.shading) {
            normals.bind(gl, BufferTarget.ARRAY);
            gl.glVertexAttribPointer(NORMAL_ATTRIB_INDEX, 3, GL.GL_FLOAT, false, 0, 0);
            gl.glEnableVertexAttribArray(NORMAL_ATTRIB_INDEX);
        }
        if (this.indices != null) {
            indices.bind(gl, BufferTarget.ELEMENT_ARRAY);
        }

        // set uniforms
        gl.glUniform1i(BILLBOARDING_UNIFORM_LOCATION, billboarding ? 1 : 0);
        gl.glUniform1i(SHADING_UNIFORM_LOCATION, shading ? 1 : 0);

        // set wireframe mode
        if (this.wireframe) {
            // enable back face culling
            gl.glDisable(GL4.GL_CULL_FACE);
            gl.glPolygonMode(GL4.GL_FRONT_AND_BACK, GL4.GL_LINE);
        } else {
            // enable back face culling
            gl.glEnable(GL4.GL_CULL_FACE);
            gl.glPolygonMode(GL4.GL_FRONT_AND_BACK, GL4.GL_FILL);
        }
    }

    public void setWireframe(boolean wireframe) {
        this.wireframe = wireframe;
    }

    public int getGLPrimitive() {
        return this.primitive.toGLValue();
    }

    public int getIndicesCount() {
        return this.indices.length();
    }

    public int getVerticesCount() {
        return this.vertices.length();
    }

    public boolean hasIndices() {
        return this.indices != null;
    }

    public VertexArrayObject(
        GL4 gl,
        Buffer vertices,
        Buffer normals,
        Buffer indices,
        Primitive primitive,
        boolean wireframe,
        boolean shading,
        boolean billboarding
    ) {
        // create a OpenGL VAO
        int[] ids = new int[1];
        gl.glGenVertexArrays(1, ids, 0);
        this.id = ids[0];

        // bind vao
        gl.glBindVertexArray(this.id);
        
        // setup vertices
        vertices.send(gl);
        vertices.bind(gl, BufferTarget.ARRAY);
        gl.glVertexAttribPointer(POSITION_ATTRIB_INDEX, 3, GL.GL_FLOAT, false, 0, 0);
        gl.glEnableVertexAttribArray(POSITION_ATTRIB_INDEX);

        // setup normals
        if (normals != null) {
            normals.send(gl);
            normals.bind(gl, BufferTarget.ARRAY);
            gl.glVertexAttribPointer(NORMAL_ATTRIB_INDEX, 3, GL.GL_FLOAT, false, 0, 0);
            gl.glEnableVertexAttribArray(NORMAL_ATTRIB_INDEX);
        }

        // setup indices
        if (indices != null) {
            indices.send(gl);
        }

        // save buffers
        this.vertices = vertices;
        this.normals = normals;
        this.indices = indices;

        // set attributes
        this.primitive = primitive;
        this.wireframe = wireframe;
        this.shading = shading;
        this.billboarding = billboarding;
    }

    public String toString() {
        String out = "VAO: \n- vertices: ";
        out += this.vertices.toStringAsFloats(3);
        if (this.indices != null) {
            out += "- indices: ";
            out += this.indices.toStringAsInts(0);
        }
        out += "- primitive: " + this.primitive + "\n";
        out += "- billboarding: " + this.billboarding + "\n";
        out += "- shading: " + this.shading + "\n";
        out += "- wireframe: " + this.wireframe + "\n";

        return out;
    }

    public void delete(GL4 gl) {
        // Delete OpenGL buffers when this gets cleaned up
        gl.glDeleteVertexArrays(1, new int[]{this.id}, 0);
    }
}
