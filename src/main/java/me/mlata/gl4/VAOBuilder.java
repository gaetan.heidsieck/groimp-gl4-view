package me.mlata.gl4;

import com.jogamp.opengl.GL4;

public class VAOBuilder {
    Buffer vertices;
    Buffer normals;
    Buffer indices;
    boolean wireframe = false;
    boolean billboarding = false;
    boolean billboardScaling = true;
    boolean shading = true;
    Primitive primitive = Primitive.Triangles;

    public Buffer getVertexBuffer(int initialSize) {
        if (this.vertices == null) {
            this.vertices = new Buffer(initialSize, BufferUsage.STATIC_DRAW);
        }
        return this.vertices;
    }

    public VAOBuilder addVertices(float[] vertices) {
        if (this.vertices == null) {
            this.vertices = new Buffer(vertices, BufferUsage.STATIC_DRAW);
        } else {
            this.vertices.put(vertices);
        }
        return this;
    }

    public VAOBuilder addVertices(Buffer vertices) {
        this.vertices = vertices;
        return this;
    }

    public Buffer getNormalBuffer(int initialSize) {
        if (this.normals == null) {
            this.normals = new Buffer(initialSize, BufferUsage.STATIC_DRAW);
        }
        return this.normals;
    }

    public VAOBuilder addNormals(float[] normals) {
        if (this.normals == null) {
            this.normals = new Buffer(normals, BufferUsage.STATIC_DRAW);
        } else {
            this.normals.put(normals);
        }
        return this;
    }

    public VAOBuilder addNormals(Buffer normals) {
        this.normals = normals;
        return this;
    }

    public Buffer getIndexBuffer(int initialSize) {
        if (this.indices == null) {
            this.indices = new Buffer(initialSize, BufferUsage.STATIC_DRAW);
        }
        return this.indices;
    }

    public VAOBuilder addIndices(int[] indices) {
        if (this.indices == null) {
            this.indices = new Buffer(indices, BufferUsage.STATIC_DRAW);
        } else {
            this.indices.put(indices);
        }
        return this;
    }

    public VAOBuilder addIndices(Buffer indices) {
        this.indices = indices;
        return this;
    }

    public VAOBuilder setPrimitive(Primitive primitive) {
        this.primitive = primitive;
        switch (primitive) {
            case Triangles:
            case TriangleStrip:
            case TriangleFan:
                this.setShading(true);
                break;
            case Lines:
            case LineStrip:
            case LineLoop:
            case Points:
                this.setShading(false);
        }
        return this;
    }

    public VAOBuilder setWireframe(boolean wireframe) {
        this.wireframe = wireframe;
        if (wireframe)
            this.shading = false;
        return this;
    }

    public VAOBuilder setBillboarding(boolean billboarding) {
        this.billboarding = billboarding;
        return this;
    }

    public VAOBuilder setBillboardScaling(boolean billboardScaling) {
        this.billboardScaling = billboardScaling;
        return this;
    }

    public VAOBuilder setShading(boolean shading) {
        this.shading = shading;
        return this;
    }

    public VertexArrayObject finish(GL4 gl) {
        return new VertexArrayObject(gl, vertices, normals, indices, primitive, wireframe, shading, billboarding);
    }
}
