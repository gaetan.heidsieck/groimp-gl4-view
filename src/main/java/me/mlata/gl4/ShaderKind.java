package me.mlata.gl4;

import com.jogamp.opengl.GL4;

public enum ShaderKind {
    Vertex,
    Fragment;

    public int toGLValue() {
        switch (this) {
            case Vertex:
                return GL4.GL_VERTEX_SHADER;
            case Fragment:
                return GL4.GL_FRAGMENT_SHADER;
            default:
                return 0;
        }
    }

    public String toString() {
        switch (this) {
            case Vertex:
                return "Vertex";
            case Fragment:
                return "Fragment";
            default:
                return ""; // not possible
        }
        
    }
}
