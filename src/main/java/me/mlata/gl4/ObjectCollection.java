package me.mlata.gl4;

import javax.vecmath.Color3f;
import javax.vecmath.Matrix4f;
import javax.vecmath.Tuple3f;

import com.jogamp.opengl.GL4;

import me.mlata.gl4.volumes.Lines;
import me.mlata.gl4.volumes.Volume;

public class ObjectCollection {
    // Store objects in chunks of up to MAX_INSTANCES
    ChunkedBuffer transforms;
    ChunkedBuffer colors;
    
    Volume volume;

    private static final int MODEL_UNIFORM_BINDING = 0;
    private static final int COLOR_UNIFORM_BINDING = 1;

    // Max number of instances supported. This has to be the same number as in the used vertex shader.
    // 256 * 16 floats is the maximum guaranteed by the specification
    public static final int CHUNK_INSTANCES = 256;
    public static final int TRANSFORM_CHUNK_SIZE = CHUNK_INSTANCES * 16;
    public static final int COLOR_CNUNK_SIZE = CHUNK_INSTANCES * 4;

    public ObjectCollection(Volume volume) {
        this.transforms = new ChunkedBuffer(TRANSFORM_CHUNK_SIZE);
        this.colors = new ChunkedBuffer(COLOR_CNUNK_SIZE);
        this.volume = volume;
    }

    protected boolean bindNext(GL4 gl, BufferTarget target) {
        this.transforms.bindNext(gl, target);
        return this.colors.bindNext(gl, target);
    }

    protected boolean bindNext(GL4 gl, BufferTarget target, int index) {
        this.transforms.bindNext(gl, target, index);
        return this.colors.bindNext(gl, target, index);
    }

    protected void resetBound(GL4 gl) {
        this.transforms.resetBound(gl);
        this.colors.resetBound(gl);
    }

    /**
     * Draws the objects in this collection using a few instanced draw calls.
     * @param gl The current OpenGL context
     * @param vao The vertex array object to use for rendering
     */
    public void draw(GL4 gl) {
        // get vao from volume
        VertexArrayObject vao = this.volume.getMesh(gl);

        if (vao.indices == null || vao.indices.length < 100) {
            System.out.println(vao);
            System.out.println("Drawing " + getCount() + " instances");
            System.out.println("Transforms: " + this.transforms.getCurrent().toStringAsFloats(4));
        }

        // bind vertex arrary object
        vao.bind(gl);

        this.transforms.resetBound(gl);
        this.colors.resetBound(gl);

        while (this.transforms.bindNext(gl, BufferTarget.UNIFORM, MODEL_UNIFORM_BINDING)) {
            this.colors.bindNext(gl, BufferTarget.UNIFORM, COLOR_UNIFORM_BINDING);

            if (vao.hasIndices()) {
                // issue draw call
            		gl.glDrawElementsInstanced(
                    vao.getGLPrimitive(),
                    vao.getIndicesCount(),
                    GL4.GL_UNSIGNED_INT,
                    0,
                    this.getCount()
                );
            } else {
                // issue draw call
                gl.glDrawArraysInstanced(
                    vao.getGLPrimitive(), 
                    0,
                    vao.getVerticesCount(),
                    this.getCount()
                );
            }
        }

        int error = gl.glGetError();
        if (error != 0) {
            System.err.println("An OpenGL error occurred: " + error);
        }
    }

    /**
     * @return The number of objects in this collection.
     */
    public int getCount() {
        return this.transforms.getTotalSize() / 16;
    }

    public Volume getVolume() {
        return this.volume;
    }

    /**
     * Adds an object to the collection.
     * This may create a new OpenGL buffer if neccessary.
     */
    public void put(GL4 gl, Matrix4f t, Tuple3f color) {
        this.transforms.put(t);
        this.colors.put(new float[] {
            color.x, color.y, color.z, 1
        });
    }

    public void send(GL4 gl) {
        this.transforms.send(gl);
        this.colors.send(gl);
    }

    public void clear() {
        this.transforms.clear();
        this.colors.clear();
    }

    protected void delete(GL4 gl) {
        this.transforms.delete(gl);
        this.colors.delete(gl);
        this.volume.deleteVAO(gl);
    }
}
