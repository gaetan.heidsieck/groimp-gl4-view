package me.mlata.gl4.volumes;

import java.nio.IntBuffer;

import javax.vecmath.Vector3f;

import me.mlata.gl4.Buffer;
import me.mlata.gl4.VAOBuilder;

public final class Sphere extends Volume {
    static final float s5 = (float) Math.sqrt(5); // square root of 5
    static final float s5i = 1 / s5; // 1 over square root of 5
    static final float s1ps52 = (float) Math.sqrt((1 + s5i) / 2);
    static final float s1ms52 = (float) Math.sqrt((1 - s5i) / 2);

    private static final int hashCode = "sphere".hashCode();

    static final int LEVELS = 3;

    @Override
    public boolean equalsInner(Object other) {
        return true;
    }

    @Override
    public int hashCodeInner() {
        return hashCode;
    }

    @Override
    public String toString() {
        return "Sphere";
    }

    private int countIcoCorners(int levels) {
        int faces = 20;
        int corners = 12;
        for (int i = 0; i < levels; i++) {
            // At each level, a vertex is added for each side of each face
            // TODO: reuse identical vertices to almost halve memory usage
            corners += faces * 3;
            faces *= 4;
        }
        return corners;
    }

    // TODO: LOD stuff and level as some parameter or attribute or something
    public void buildMesh(VAOBuilder builder) {
        Buffer vertices = builder.getVertexBuffer(3 * countIcoCorners(LEVELS));
        builder.addNormals(vertices);
        vertices.put(new float[]{
            1, 0, 0,
            -1, 0, 0,
            s5i, 2 * s5i, 0,
            -s5i, -2 * s5i, 0,
            s5i, (1 - s5i) / 2, s1ps52,
            -s5i, -(1 - s5i) / 2, s1ps52,
            s5i, (1 - s5i) / 2, -s1ps52,
            -s5i, -(1 - s5i) / 2, -s1ps52,
            s5i, -(1 + s5i) / 2, s1ms52,
            -s5i, (1 + s5i) / 2, s1ms52,
            s5i, -(1 + s5i) / 2, -s1ms52,
            -s5i, (1 + s5i) / 2, -s1ms52,
        });
        int[] indices = {
            4, 5, 8,
            8, 5, 3,
            3, 5, 1,
            1, 5, 9,
            9, 5, 4,

            0, 6, 2,
            2, 6, 11,
            11, 6, 7,
            7, 6, 10,
            10, 6, 0,

            0, 4, 8,
            4, 0, 2,
            2, 9, 4,
            9, 2, 11,
            11, 1, 9, 
            1, 11, 7,
            7, 3, 1,
            3, 7, 10,
            10, 8, 3,
            8, 10, 0
        };
        for (int i = 0; i < LEVELS; i++) {
            // TODO: do not allocate a new buffer for each iteration
            IntBuffer newIndices = IntBuffer.allocate(indices.length * 4);
            for (int triangle = 0; triangle < (indices.length / 3); triangle++) {
                int ai = indices[3 * triangle];
                int bi = indices[3 * triangle + 1];
                int ci = indices[3 * triangle + 2];
                Vector3f a = new Vector3f(vertices.getFloat(3*ai), vertices.getFloat(3*ai + 1), vertices.getFloat(3*ai + 2));
                Vector3f b = new Vector3f(vertices.getFloat(3*bi), vertices.getFloat(3*bi + 1), vertices.getFloat(3*bi + 2));
                Vector3f c = new Vector3f(vertices.getFloat(3*ci), vertices.getFloat(3*ci + 1), vertices.getFloat(3*ci + 2));
                Vector3f d = new Vector3f(a);
                d.add(b);
                d.normalize();
                Vector3f e = new Vector3f(b);
                e.add(c);
                e.normalize();
                Vector3f f = new Vector3f(c);
                f.add(a);
                f.normalize();
                int di = vertices.position() / 3;
                int ei = di + 1;
                int fi = ei + 1;
                vertices.put(new float[] {
                    d.x, d.y, d.z,
                    e.x, e.y, e.z,
                    f.x, f.y, f.z
                });
                newIndices.put(new int[] {
                    ai, di, fi,
                    di, bi, ei,
                    fi, ei, ci,
                    di, ei, fi
                });
            }
            indices = newIndices.array();
        }
        builder.addIndices(indices);
    }
}
