package me.mlata.gl4.volumes;

import me.mlata.gl4.VAOBuilder;

public class Cube extends Volume {
    private static final int hashCode = "cube".hashCode();

    @Override
    public boolean equalsInner(Object other) {
        return true;
    }

    @Override
    public int hashCodeInner() {
        return hashCode;
    }

    @Override
    public String toString() {
        return "Cube";
    }

    public void buildMesh(VAOBuilder builder) {
        // TODO: fix coordinates in model space
        // seperate vertices for each side so normals can be correct
        builder.addVertices(new float[] {
            // top
            -0.5f, 1, 0.5f,
            -0.5f, 1, -0.5f,
            0.5f, 1, -0.5f,
            0.5f, 1, 0.5f,

            // bottom
            -0.5f, 0, -0.5f,
            -0.5f, 0, 0.5f,
            0.5f, 0, 0.5f,
            0.5f, 0, -0.5f,

            // front
            -0.5f, 0, 0.5f,
            -0.5f, 1, 0.5f,
            0.5f, 1, 0.5f,
            0.5f, 0, 0.5f,

            // back
            -0.5f, 1, -0.5f,
            -0.5f, 0, -0.5f,
            0.5f, 0, -0.5f,
            0.5f, 1, -0.5f,

            // left
            -0.5f, 0, -0.5f,
            -0.5f, 1, -0.5f,
            -0.5f, 1, 0.5f,
            -0.5f, 0, 0.5f,
            
            // right
            0.5f, 0, 0.5f,
            0.5f, 1, 0.5f,
            0.5f, 1, -0.5f,
            0.5f, 0, -0.5f,
        });
        builder.addNormals(new float[] {
            // top
            0, 1, 0,
            0, 1, 0,
            0, 1, 0,
            0, 1, 0,

            // bottom
            0, -1, 0,
            0, -1, 0,
            0, -1, 0,
            0, -1, 0,

            // front
            0, 0, 1,
            0, 0, 1,
            0, 0, 1,
            0, 0, 1,

            // back
            0, 0, -1,
            0, 0, -1,
            0, 0, -1,
            0, 0, -1,

            // left
            -1, 0, 0,
            -1, 0, 0,
            -1, 0, 0,
            -1, 0, 0,

            // right
            1, 0, 0,
            1, 0, 0,
            1, 0, 0,
            1, 0, 0
        });
        builder.addIndices(new int[] {
            // top 
            0, 2, 1,
            0, 3, 2,

            // bottom
            4, 6, 5,
            4, 7, 6,

            // front
            8, 10, 9,
            8, 11, 10,

            // back
            12, 14, 13,
            12, 15, 14,

            // left,
            16, 18, 17,
            16, 19, 18,

            // right,
            20, 22, 21,
            20, 23, 22
        });
    }
}
