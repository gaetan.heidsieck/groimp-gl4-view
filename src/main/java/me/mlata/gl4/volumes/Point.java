package me.mlata.gl4.volumes;

import java.util.Objects;

import me.mlata.gl4.Primitive;
import me.mlata.gl4.VAOBuilder;

public class Point extends Volume {
    public int hashCodeInner() {
        return Objects.hash("point");
    }

    public boolean equalsInner(Object other) {
        return true;
    }

    public void buildMesh(VAOBuilder builder) {
        builder.addVertices(new float[] {
            0, 0, 0,
        });

        builder.setPrimitive(Primitive.Points);
    }
}
