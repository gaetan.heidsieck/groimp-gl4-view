package me.mlata.gl4.volumes;

import java.util.Objects;

import de.grogra.imp3d.LineArray;
import me.mlata.gl4.Buffer;
import me.mlata.gl4.Primitive;
import me.mlata.gl4.VAOBuilder;

public class Lines extends Volume {
    LineArray larray;

    public Lines() {}
    
    public Lines(LineArray lines) {
        this.larray = lines; 
    }
   
    public boolean equalsInner(Object other) {
        return this.larray.equals(((Lines) other).larray); 
    }

    public int hashCodeInner() {
        return Objects.hash("lines", this.larray);
    }

    protected void buildMesh(VAOBuilder builder) {
        if (this.larray != null) {
            builder.addVertices(this.larray.vertices.toArray());
            Buffer indices = builder.getIndexBuffer(this.larray.lines.size);
            for (int i = 0; i < this.larray.lines.size; i++) {
                int value = this.larray.lines.get(i);
                indices.put(value < 0 ? -1 : value);
            }
        } else {
            builder.addVertices(new float[] {
                0, 0, 0,
                1, 0, 0,
            });
        }
        builder.setPrimitive(Primitive.LineStrip);
    }
}
