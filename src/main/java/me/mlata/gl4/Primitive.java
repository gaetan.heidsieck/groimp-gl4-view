package me.mlata.gl4;

import com.jogamp.opengl.GL4;

public enum Primitive {
    Triangles,
    TriangleStrip,
    TriangleFan,
    Lines,
    LineStrip,
    LineLoop,
    Points;

    public int toGLValue() {
        switch (this) {
            case Triangles:
                return GL4.GL_TRIANGLES;
            case TriangleStrip:
                return GL4.GL_TRIANGLE_STRIP;
            case TriangleFan:
                return GL4.GL_TRIANGLE_FAN;
            case Lines:
                return GL4.GL_LINES;
            case LineStrip:
                return GL4.GL_LINE_STRIP;
            case LineLoop:
                return GL4.GL_LINE_LOOP;
            case Points:
                return GL4.GL_POINTS;
            default:
                return 0;
        }
    }
}
