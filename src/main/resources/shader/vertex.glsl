#version 460

layout(location = 0) in vec3 v_pos;
layout(location = 1) in vec3 v_normal;

// position in world space
out vec3 position;
out vec3 normal;
out vec3 color;

layout(location = 0) uniform mat4 view;
layout(location = 6) uniform mat4 clip;
layout(location = 1) uniform mat4 viewclip;
layout(location = 5) uniform bool billboarding;

layout(std140, binding = 0) uniform ModelMatrixBlock {
    mat4 model[256];
} instanceModelData;
layout(std140, binding = 1) uniform ColorBlock {
    vec4 color[256];
} instanceColorData;

void main() {
    if (billboarding) {
        mat4 model = instanceModelData.model[gl_InstanceID];
        vec3 translation = (view * model)[3].xyz;
        position = v_pos + translation;
        normal = normalize(v_normal);
        color = vec3(instanceColorData.color[gl_InstanceID]);

        gl_Position = clip * vec4(position, 1);
    } else {
        mat4 model = instanceModelData.model[gl_InstanceID];
        position = (model * vec4(v_pos, 1)).xyz;
        normal = normalize(transpose(inverse(mat3(model))) * v_normal);
        color = vec3(instanceColorData.color[gl_InstanceID]);

        vec4 clip_position = viewclip * model * vec4(v_pos, 1);
        gl_Position = clip_position;
    }
}
